# Game Server

The game server we use is actually the replication server which comes with UE3 by default
and luckily is provided by the latest BL:R clients.

## Setup

The following steps are required in order to host a game server:

1. **make sure you have a public IPv4 address** ([WhatIsMyIP](https://whatismyip.host/))
1. [setup the launcher](/guides/user/getting-started.md#launcher-setup)
1. add firewall rules for `BLR-Server.exe` and `Launcher.exe` (default ports are `7777, 8080`)
1. add port forwarding inside your router

## Launch server

1. goto `Server Utils` tab
1. fill the `Game Server` form  (see [Game Server/Parameters](parameters.md))
    - **if you want to make your server public available, enable `Publish` checkbox** 
1. click `Launch`

The server will appear in the *status server* list after it has been initialized (*only published servers*).

## Status Server

The status server is responsible for providing your runnning game servers status informations
to the super server and the launcher.

## How it works

BLRevive utilizes the builtin replication server of the BL:R client (included in all clients after parity patch)
and provide the *status server* which handles server registration/management.

This will happen when launching a game server with `Publish` enabled:

1. launcher starts the BL:R replication server as seperate process
1. status server gets started
   1. launches a simple http server with json api in the background
   2. make sure it is available under your public **IPv4** (IPv6 isn't supported)
   3. collect game server status from json data inside game folders (generated by [BLRevive/Tools/ServerUtil](https://gitlab.com/blrevive/tools/serverutil))